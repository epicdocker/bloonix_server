#!/bin/bash

declare -g -r BLOONIX_SERVER_PKI="/etc/bloonix/server/pki"
declare -g -r BLOONIX_WEBGUI_PKI="/etc/bloonix/webgui/pki"
declare -g -r BLOONIX_INITIALIZED_FILE="/etc/bloonix/initialized"
export BLOONIX_INITIALIZED_FILE

#### Create Bloonix server certificates
#### Extract from the file 'usr/bin/bloonix-init-server' from the package 'bloonix-server_0.66-1_all.deb'
#### For security reasons, the certificates are only generated when the container is started up and stored in the volume.
if [ ! -e "$BLOONIX_SERVER_PKI" ] ; then
    echo "[INFO] $(date -Iseconds) Create $BLOONIX_SERVER_PKI/*"
    mkdir -p $BLOONIX_SERVER_PKI
    chown root:bloonix $BLOONIX_SERVER_PKI
    chmod 750 $BLOONIX_SERVER_PKI
    openssl req -new -x509 -nodes -out $BLOONIX_SERVER_PKI/server.cert -keyout $BLOONIX_SERVER_PKI/server.key -batch
    chown root:bloonix $BLOONIX_SERVER_PKI/server.key $BLOONIX_SERVER_PKI/server.cert
    chmod 640 $BLOONIX_SERVER_PKI/server.key $BLOONIX_SERVER_PKI/server.cert
fi

#### Create Bloonix webgui certificates
#### Extract from the file 'usr/bin/bloonix-init-webgui' from the package 'bloonix-webgui-core_0.20-1_all.deb'
#### For security reasons, the certificates are only generated when the container is started up and stored in the volume.
if [ ! -e "$BLOONIX_WEBGUI_PKI" ] ; then
    echo "[INFO] $(date -Iseconds) Create $BLOONIX_WEBGUI_PKI/*"
    mkdir -p $BLOONIX_WEBGUI_PKI
    chown root:root $BLOONIX_WEBGUI_PKI
    chmod 750 $BLOONIX_WEBGUI_PKI
    openssl req -new -x509 -nodes -out $BLOONIX_WEBGUI_PKI/server.cert -keyout $BLOONIX_WEBGUI_PKI/server.key -batch
    chown root:root $BLOONIX_WEBGUI_PKI/server.key $BLOONIX_WEBGUI_PKI/server.cert
    chmod 640 $BLOONIX_WEBGUI_PKI/server.key $BLOONIX_WEBGUI_PKI/server.cert
fi

#### Waiting for Elasticsearch
echo "[INFO] $(date -Iseconds) Waiting for Elasticsearch ..."
wait-for-it -h $BLOONIX_ELASTICSEARCH_HOST -p $BLOONIX_ELASTICSEARCH_PORT -s -t 60 

#### waiting for PostgreSQL
echo "[INFO] $(date -Iseconds) Waiting for PostgreSQL ..."
wait-for-it -h $BLOONIX_DATABASE_HOST -p $BLOONIX_DATABASE_PORT -s -t 60

#### Create configuration
if [[ $BLOONIX_DATABASE_CONFIGURATION_SKIP == false ]]; then 
  /bin/bash /etc/epicsoft/database.conf.sh
fi
if [[ $BLOONIX_SERVER_CONFIGURATION_SKIP == false ]]; then
  /bin/bash /etc/epicsoft/server.conf.sh
fi 
if [[ $BLOONIX_SRVCHK_CONFIGURATION_SKIP == false ]]; then 
  /bin/bash /etc/epicsoft/srvchk.conf.sh
fi 
if [[ $BLOONIX_WEBGUI_CONFIGURATION_SKIP == false ]]; then
  /bin/bash /etc/epicsoft/webgui.conf.sh
fi

#### First run of container 
if [ ! -f $BLOONIX_INITIALIZED_FILE ]; then
  /bin/bash /etc/epicsoft/firststart.sh
fi

#### Bloonix pre-start 
echo "[INFO] $(date -Iseconds) Starting Bloonix pre-start"
/usr/lib/bloonix/bin/bloonix-pre-start /var/lib/bloonix /var/lib/bloonix/ipc /var/lib/bloonix/webgui /var/log/bloonix /var/run/bloonix

#### Bloonix web-gui
echo "[INFO] $(date -Iseconds) Starting Bloonix Web-GUI"
/srv/bloonix/webgui/scripts/bloonix-webgui --pid-file /var/run/bloonix/bloonix-webgui.pid --config-file /etc/bloonix/webgui/main.conf

#### Bloonix server 
echo "[INFO] $(date -Iseconds) Starting Bloonix server"
/usr/bin/bloonix-server --pid-file /var/run/bloonix/bloonix-server.pid --config-file /etc/bloonix/server/main.conf

#### Bloonix server-check
echo "[INFO] $(date -Iseconds) Starting Bloonix server-check"
/usr/bin/bloonix-srvchk --pid-file /var/run/bloonix/bloonix-srvchk.pid --config-file /etc/bloonix/srvchk/main.conf

#### Nginx Server
echo "[INFO] $(date -Iseconds) Starting Nginx"
nginx

#### Bloonix logs
echo "[INFO] $(date -Iseconds) Bloonix logs ..."
tail -qF /var/log/bloonix/*.log