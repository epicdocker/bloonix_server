#!/bin/bash

declare -g -r BLOONIX_WEBGUI_CONF_TPL="/etc/bloonix.template/webgui.conf.tpl"
declare -g -r BLOONIX_WEBGUI_CONF_FILE="/etc/bloonix/webgui/main.conf"
declare -r BLOONIX_WEBGUI_CONF_DIR=$(dirname "${BLOONIX_WEBGUI_CONF_FILE}")

#### Create database config directory and set user, group and right 
mkdir -p $BLOONIX_WEBGUI_CONF_DIR
chmod 750 $BLOONIX_WEBGUI_CONF_DIR
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_WEBGUI_CONF_DIR
#### Load 'mo' functions
. /opt/mo/mo
#### Create configuration file and set user, group and right 
cat $BLOONIX_WEBGUI_CONF_TPL | mo --fail-not-set > $BLOONIX_WEBGUI_CONF_FILE
chmod 750 $BLOONIX_WEBGUI_CONF_FILE
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_WEBGUI_CONF_FILE

echo "[INFO] $(date -Iseconds) Created webgui configuration file '$BLOONIX_WEBGUI_CONF_FILE'"

#### Nginx 

declare -r BLOONIX_NGINX_CONF_TPL="/etc/bloonix.template/nginx.conf"
declare -r BLOONIX_NGINX_CONF_FILE="/etc/bloonix/webgui/nginx.conf"

if [[ ! -f $BLOONIX_NGINX_CONF_FILE ]]; then 
  echo "[WARN] $(date -Iseconds) Nginx configuration file not found '$BLOONIX_NGINX_CONF_FILE'"
  #### Copy nginx configuration file from template and set user, group and right 
  cp $BLOONIX_NGINX_CONF_TPL $BLOONIX_NGINX_CONF_FILE
  chmod 750 $BLOONIX_NGINX_CONF_FILE
  chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_NGINX_CONF_FILE
  echo "[INFO] $(date -Iseconds) Created nginx configuration file '$BLOONIX_NGINX_CONF_FILE'"
fi
