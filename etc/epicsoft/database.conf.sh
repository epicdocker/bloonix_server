#!/bin/bash

declare -g -r BLOONIX_DB_CONF_TPL="/etc/bloonix.template/database.conf.tpl"
declare -g -r BLOONIX_DB_CONF_FILE="/etc/bloonix/database/main.conf"
declare -r BLOONIX_DB_CONF_DIR=$(dirname "${BLOONIX_DB_CONF_FILE}")

declare -g -r BLOONIX_DB_HOST=$BLOONIX_DATABASE_HOST
declare -g -r BLOONIX_DB_PORT=$BLOONIX_DATABASE_PORT
declare -g -r BLOONIX_DB_SCHEMA=$BLOONIX_DATABASE_SCHEMA
declare -g -r BLOONIX_DB_USER=$([ -f "$BLOONIX_DATABASE_USER_FILE" ] && cat $BLOONIX_DATABASE_USER_FILE || echo "$BLOONIX_DATABASE_USER")
declare -g -r BLOONIX_DB_PASS=$([ -f "$BLOONIX_DATABASE_PASSWORD_FILE" ] && cat $BLOONIX_DATABASE_PASSWORD_FILE || echo "$BLOONIX_DATABASE_PASSWORD")

declare -r BLOONIX_DATABASE_TYPE_LOWER=$(echo $BLOONIX_DATABASE_TYPE | tr '[:upper:]' '[:lower:]')
declare -g BLOONIX_DB_DRIVER
if [[ "$BLOONIX_DATABASE_TYPE_LOWER" = *"postgres"* || "$BLOONIX_DATABASE_TYPE_LOWER" = *"pg"* ]]; then 
  BLOONIX_DB_DRIVER="Pg"
elif [[ "$BLOONIX_DATABASE_TYPE_LOWER" = *"mysql"* ]]; then 
  BLOONIX_DB_DRIVER="mysql"
else 
  echo "[WARN] $(date -Iseconds) No suitable database type found"
  BLOONIX_DB_DRIVER=$BLOONIX_DATABASE_TYPE  
fi
echo "[INFO] $(date -Iseconds) Use database type '$BLOONIX_DB_DRIVER'"
echo "[INFO] $(date -Iseconds) Use database host '$BLOONIX_DB_HOST'"
echo "[INFO] $(date -Iseconds) Use database port '$BLOONIX_DB_PORT'"
echo "[INFO] $(date -Iseconds) Use database schema '$BLOONIX_DATABASE_SCHEMA'"

#### Create database config directory and set user, group and right 
mkdir -p $BLOONIX_DB_CONF_DIR
chmod 750 $BLOONIX_DB_CONF_DIR
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_DB_CONF_DIR
#### Load 'mo' functions
. /opt/mo/mo
#### Create configuration file and set user, group and right 
cat $BLOONIX_DB_CONF_TPL | mo --fail-not-set > $BLOONIX_DB_CONF_FILE
chmod 750 $BLOONIX_DB_CONF_FILE
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_DB_CONF_FILE

echo "[INFO] $(date -Iseconds) Created database configuration file '$BLOONIX_DB_CONF_FILE'"
