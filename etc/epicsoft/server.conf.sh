#!/bin/bash

declare -g -r BLOONIX_SERVER_CONF_TPL="/etc/bloonix.template/server.conf.tpl"
declare -g -r BLOONIX_SERVER_CONF_FILE="/etc/bloonix/server/main.conf"
declare -r BLOONIX_SERVER_CONF_DIR=$(dirname "${BLOONIX_SERVER_CONF_FILE}")

#### Create server config directory and set user, group and right 
mkdir -p $BLOONIX_SERVER_CONF_DIR
chmod 750 $BLOONIX_SERVER_CONF_DIR
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_SERVER_CONF_DIR
#### Load 'mo' functions
. /opt/mo/mo
#### Create configuration file and set user, group and right 
cat $BLOONIX_SERVER_CONF_TPL | mo --fail-not-set > $BLOONIX_SERVER_CONF_FILE
chmod 750 $BLOONIX_SERVER_CONF_FILE
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_SERVER_CONF_FILE

echo "[INFO] $(date -Iseconds) Created server configuration file '$BLOONIX_SERVER_CONF_FILE'"
