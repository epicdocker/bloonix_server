#!/bin/bash 

declare CUSTOM_APT_PARAM_FORCE_IPV4=""
if [[ $BLOONIX_PLUGINS_CONFIGURATION_FORCE_IPV4 == true ]]; then
  CUSTOM_APT_PARAM_FORCE_IPV4="-o Acquire::ForceIPv4=true"
fi

echo "[INFO] $(date -Iseconds) Prepare for the first start of Bloonix ..."
echo "[INFO] $(date -Iseconds) Initialize Elasticsearch ..."
/srv/bloonix/webgui/schema/init-elasticsearch $BLOONIX_ELASTICSEARCH_HOST:$BLOONIX_ELASTICSEARCH_PORT

echo "[INFO] Configure plugins ..."
apt-get -y $CUSTOM_APT_PARAM_FORCE_IPV4 update 
apt-get -y $CUSTOM_APT_PARAM_FORCE_IPV4 install bloonix-plugin-config
apt-get -y autoclean
apt-get -y autoremove
rm -rf /var/lib/apt/lists/*

echo "[INFO] $(date -Iseconds) Prepare for the first start is done"
echo $(date) > $BLOONIX_INITIALIZED_FILE
