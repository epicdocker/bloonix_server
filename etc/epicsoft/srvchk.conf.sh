#!/bin/bash

declare -g -r BLOONIX_SRVCHK_CONF_TPL="/etc/bloonix.template/srvchk.conf.tpl"
declare -g -r BLOONIX_SRVCHK_CONF_FILE="/etc/bloonix/srvchk/main.conf"
declare -r BLOONIX_SRVCHK_CONF_DIR=$(dirname "${BLOONIX_SRVCHK_CONF_FILE}")

#### Create srvchk config directory and set user, group and right 
mkdir -p $BLOONIX_SRVCHK_CONF_DIR
chmod 750 $BLOONIX_SRVCHK_CONF_DIR
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_SRVCHK_CONF_DIR
#### Load 'mo' functions
. /opt/mo/mo
#### Create configuration file and set user, group and right 
cat $BLOONIX_SRVCHK_CONF_TPL | mo --fail-not-set > $BLOONIX_SRVCHK_CONF_FILE
chmod 750 $BLOONIX_SRVCHK_CONF_FILE
chown $BLOONIX_SERVER_USER:$BLOONIX_SERVER_GROUP $BLOONIX_SRVCHK_CONF_FILE

echo "[INFO] $(date -Iseconds) Created srvchk configuration file '$BLOONIX_SRVCHK_CONF_FILE'"
