#!/bin/sh

#### Bloonix server 
ps -ax | grep "[/]usr/bin/perl /usr/bin/bloonix-server" > /dev/null || exit 2

#### Bloonix server check 
ps -ax | grep "[/]usr/bin/perl /usr/bin/bloonix-srvchk" > /dev/null || exit 3

#### Bloonix Web-GUI
ps -ax | grep "[/]usr/bin/perl /srv/bloonix/webgui/scripts/bloonix-webgui" > /dev/null || exit 4
curl --fail --silent --insecure https://localhost:443 > /dev/null || exit 5

#### nginx
ps -ax | grep "[n]ginx: master process" > /dev/null || exit 6
