driver {{BLOONIX_DB_DRIVER}}
host {{BLOONIX_DB_HOST}}
port {{BLOONIX_DB_PORT}}
database {{BLOONIX_DB_SCHEMA}}
user {{BLOONIX_DB_USER}}
password {{BLOONIX_DB_PASS}}
sth_cache_enabled yes