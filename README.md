<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Requirements](#Requirements)
* 3. [First steps](#Firststeps)
* 4. [Examples](#Examples)
	* 4.1. [Docker Compose v2](#DockerComposev2)
		* 4.1.1. [Start Docker Compose](#StartDockerCompose)
		* 4.1.2. [Stop Docker Compose](#StopDockerCompose)
	* 4.2. [Docker Swarm](#DockerSwarm)
		* 4.2.1. [Deploy](#Deploy)
		* 4.2.2. [Cleanup](#Cleanup)
	* 4.3. [Bloonix server with agent](#Bloonixserverwithagent)
* 5. [Environments ##](#Environments)
* 6. [Limitations](#Limitations)
* 7. [Configuration](#Configuration)
	* 7.1. [Custom configuration](#Customconfiguration)
	* 7.2. [Further configuration parameters](#Furtherconfigurationparameters)
* 8. [Links](#Links)
* 9. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Bloonix server with webgui

Docker Image for [Bloonix Server](https://bloonix-monitoring.org/) with Webgui.


##  1. <a name='Versions'></a>Versions

The version numbers are separate versions and have no connection with the used applications in the Docker image. In the description you can see the version numbers of the integrated applications.

`latest` [Dockerfile](https://gitlab.com/epicdocker/bloonix_server/blob/master/Dockerfile)

`1.1.3` [Dockerfile](https://gitlab.com/epicdocker/bloonix_server/blob/release-1.1.3/Dockerfile)

`1.1.2` [Dockerfile](https://gitlab.com/epicdocker/bloonix_server/blob/release-1.1.2/Dockerfile)

`1.1.0` [Dockerfile](https://gitlab.com/epicdocker/bloonix_server/blob/release-1.1.0/Dockerfile)

`1.0.0` [Dockerfile](https://gitlab.com/epicdocker/bloonix_server/blob/release-1.0.0/Dockerfile)


###  1.1. <a name='Details'></a>Details

`1.1.3` `latest`

- See version `1.1.2`
- Used the [Bloonix base image](https://gitlab.com/epicdocker/bloonix_base/tree/release-1.1.4/) in version `1.1.4` as base image.

`1.1.2`

- See version `1.1.0`
- Added environment `BLOONIX_PLUGINS_CONFIGURATION_FORCE_IPV4` for Docker without IPv6 support.
- Create server, webgui, database and srvchk config directories and set user, group and right.
- Creates nginx configuration if it does not exist.

`1.1.0`

- Used the [Bloonix base image](https://gitlab.com/epicdocker/bloonix_base/tree/release-1.1.2/) in version `1.1.2` as base image.
- Used Bloonix server in version `0.66-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).
- Used Bloonix webgui in version `0.131-1` from [Bloonix repository](https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/).


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Instance of PostgreSQL or MySQL database - see prepared PostgreSQL database - https://gitlab.com/epicdocker/bloonix_postgresql
- Instance of elasticsearch in version `2` or `5` - https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
- Optional: Docker Swarm initialized - https://docs.docker.com/engine/swarm/ - Some examples use Docker Swarm, but are not required
- Optional: Docker Compose installed (is needed for examples) - https://docs.docker.com/compose/


##  3. <a name='Firststeps'></a>First steps

- The web interface is accessible on port `443`. Call the URL in the browser `https://localhost/`.
- For the first login use the username _(Email)_ `admin` and the password `admin`. A request to change the password occurs immediately after successful login.


##  4. <a name='Examples'></a>Examples


###  4.1. <a name='DockerComposev2'></a>Docker Compose v2 


####  4.1.1. <a name='StartDockerCompose'></a>Start Docker Compose 

    docker-compose -f examples/server_compose_v2.yml up


####  4.1.2. <a name='StopDockerCompose'></a>Stop Docker Compose

Press `Ctrl` + `c`


###  4.2. <a name='DockerSwarm'></a>Docker Swarm


####  4.2.1. <a name='Deploy'></a>Deploy 

    docker stack deploy -c examples/server_compose_v3.yml bloonix


####  4.2.2. <a name='Cleanup'></a>Cleanup 

    docker stack rm bloonix


###  4.3. <a name='Bloonixserverwithagent'></a>Bloonix server with agent

See [simple example to add Bloonix agent](https://gitlab.com/epicdocker/bloonix_agent/blob/master/examples/simple_example_add_agent.md).


##  5. <a name='Environments'></a>Environments ##

`BLOONIX_DATABASE_TYPE`

*default:* PostgreSQL

Specifies the driver to use for the database: `PostgreSQL` or `MySQL` is possible.

`BLOONIX_DATABASE_HOST`

*default:* postgres

Hostname or servicename of the database server.

`BLOONIX_DATABASE_PORT`

*default:* 5432

Database server port. By default, the PostgreSQL port is used.

`BLOONIX_DATABASE_SCHEMA`

*default:* bloonix

Used database schema

`BLOONIX_DATABASE_USER`

*default:* bloonix

Database username

`BLOONIX_DATABASE_USER_FILE`

*default:* _empty_

Database username as [Docker secret](https://docs.docker.com/engine/reference/commandline/secret/). If specified, parameter `BLOONIX_DATABASE_USER` is ignored.

`BLOONIX_DATABASE_PASSWORD`

*default:* _empty_

IMPORTANT: Plaintext details of passwords are unsecure. Better use `BLOONIX_DATABASE_PASSWORD_FILE` with [Docker secret](https://docs.docker.com/engine/reference/commandline/secret/).

`BLOONIX_DATABASE_PASSWORD_FILE`

*default:* _empty_

Database password as [Docker secret](https://docs.docker.com/engine/reference/commandline/secret/). If specified, parameter `BLOONIX_DATABASE_PASSWORD` is ignored.

`BLOONIX_ELASTICSEARCH_PROTO`

*default:* http

The protocol to use for Elasticsearch. Allowed is `http` or `https`.

`BLOONIX_ELASTICSEARCH_HOST`

*default:* elasticsearch

Hostname or servicename of the Elasticsearch server.

`BLOONIX_ELASTICSEARCH_PORT`

*default:* 9200

Elasticsearch server port. By default, the Elasticsearch REST port is used.

`BLOONIX_ELASTICSEARCH_TIMEOUT`

*default:* 60

Elasticsearch connection timeout in seconds.

`BLOONIX_ELASTICSEARCH_MODE`

*default:* balanced

The requests to Elasticsearch can be balanced or switched on errors.

`BLOONIX_PLUGINS_CONFIGURATION_FORCE_IPV4`

*default:* false

Forces plugin configuration to use IPv4. This parameter should be set to `true` if Docker does not support IPv6 connections and thus hangs Bloonix server container on startup.

`BLOONIX_SERVER_VERSION` _Used only when building Docker image_

*default:* see version details

Specifies the Bloonix server version to install.

`BLOONIX_WEBGUI_VERSION` _Used only when building Docker image_

*default:* see version details

_Used only when building Docker image_

Specifies the Bloonix webgui version to install.

`BLOONIX_DATABASE_CONFIGURATION_SKIP`

*default:* false

Disable automatic database configuration

`BLOONIX_SERVER_CONFIGURATION_SKIP`

*default:* false

Disable automatic Bloonix server configuration

`BLOONIX_SRVCHK_CONFIGURATION_SKIP`

*default:* false

Disable automatic Bloonix srvchk configuration

`BLOONIX_WEBGUI_CONFIGURATION_SKIP`

*default:* false

Disable automatic Bloonix webgui configuration


##  6. <a name='Limitations'></a>Limitations

- The Docker image for Bloonix server is not intended for cluster operation and must be run with only one active instance `replicas = 1` _(send me the solution)_.
- E-mail delivery is currently not configurable _(being worked on)_ _(possible via manual configuration)_.
- SMS delivery is currently not configurable _(possible via manual configuration)_.
- Log files are written inside the container, stdout does not happen.


##  7. <a name='Configuration'></a>Configuration

The configuration templates are located in directory `/etc/bloonix.template` and are regenerated each time when the container is started.


###  7.1. <a name='Customconfiguration'></a>Custom configuration

To change existing configuration, you can overwrite the directory `/etc/bloonix.template` with your own templates. The current templates, see [Source code](https://gitlab.com/epicdocker/bloonix_server/tree/master/etc/bloonix.template), can you take and change. When the container starting, configurations are created from the templates.


###  7.2. <a name='Furtherconfigurationparameters'></a>Further configuration parameters

For a description, see the template or the Bloonix configuration file.

`BLOONIX_SERVER_USER`

*default:* bloonix

`BLOONIX_SERVER_GROUP`

*default:* bloonix

`BLOONIX_SERVER_TIMEZONE`

*default:* Europe/Berlin

`BLOONIX_SERVER_HOSTNAME`

*default:* _empty_ _(the system hostname)_

`BLOONIX_SERVER_ALLOW_FROM`

*default:* 127.0.0.1/32

`BLOONIX_SERVER_TCP_PORT`

*default:* 5460

`BLOONIX_SERVER_TCP_USE_SSL`

*default:* yes

`BLOONIX_SERVER_STATUS_ENABLED`

*default:* yes

`BLOONIX_SERVER_STATUS_ALLOW_FROM`

*default:* 127.0.0.1

`BLOONIX_SRVCHK_SSL_VERIFY_MODE`

*default:* none

`BLOONIX_WEBGUI_FCGI_PORT`

*default:* 9000

`BLOONIX_WEBGUI_WEBAPP_ENABLE_USER_TRACKING`

*default:* no

Further parameters can be extended if necessary or write to me.


##  8. <a name='Links'></a>Links 

- Bloonix server Docker image - https://gitlab.com/epicdocker/bloonix_server
- Bloonix server Docker registry - https://gitlab.com/epicdocker/bloonix_server/container_registry
- Bloonix PostgreSQL Docker image _(preinitialised)_ - https://gitlab.com/epicdocker/bloonix_postgresql
- Bloonix agent Docker image - https://gitlab.com/epicdocker/bloonix_agent
- Bloonix satellite Docker image - https://gitlab.com/epicdocker/bloonix_satellite
- Bloonix base Docker image - https://gitlab.com/epicdocker/bloonix_base
- Bloonix Repository - https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/
- Bloonix OpenSource Monitoring Software - https://bloonix-monitoring.org/


##  9. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/bloonix_server/blob/master/LICENSE)
