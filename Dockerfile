FROM registry.gitlab.com/epicdocker/bloonix_base:1.1.4
LABEL image.name="bloonix_server" \
      image.description="Docker image for Bloonix server with webgui" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2018 epicsoft.de / Alexander Schwarz" \
      license="MIT"

#### https://download.bloonix.de/repos/debian/dists/stretch/main/binary-amd64/
ENV BLOONIX_SERVER_VERSION="0.66-1" \
    BLOONIX_WEBGUI_VERSION="0.132-1" \
    BLOONIX_DATABASE_CONFIGURATION_SKIP=false \
    BLOONIX_SERVER_CONFIGURATION_SKIP=false \
    BLOONIX_SRVCHK_CONFIGURATION_SKIP=false \
    BLOONIX_WEBGUI_CONFIGURATION_SKIP=false

ENV BLOONIX_DATABASE_TYPE="PostgreSQL" \
    BLOONIX_DATABASE_HOST="postgres" \
    BLOONIX_DATABASE_PORT="5432" \
    BLOONIX_DATABASE_SCHEMA="bloonix" \
    BLOONIX_DATABASE_USER="bloonix" \
    BLOONIX_DATABASE_USER_FILE="" \
    BLOONIX_DATABASE_PASSWORD="" \
    BLOONIX_DATABASE_PASSWORD_FILE="" \
    BLOONIX_ELASTICSEARCH_PROTO="http" \
    BLOONIX_ELASTICSEARCH_HOST="elasticsearch" \
    BLOONIX_ELASTICSEARCH_PORT="9200" \
    BLOONIX_ELASTICSEARCH_TIMEOUT="60" \
    BLOONIX_ELASTICSEARCH_MODE="balanced" \
    BLOONIX_PLUGINS_CONFIGURATION_FORCE_IPV4=false

RUN apt-get -y update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get -y install nginx \
                       postfix \
                       sensible-utils \
                       curl \
                       procps \
                       bloonix-server=${BLOONIX_SERVER_VERSION} \
                       bloonix-webgui=${BLOONIX_WEBGUI_VERSION} \
                       bloonix-plugins-basic \
                       bloonix-plugins-linux \
 && rm /etc/nginx/sites-enabled/default \
 && rm -rf /etc/bloonix/server/pki \
 && rm -rf /etc/bloonix/webgui/pki \
 && apt-get -y autoclean \
 && apt-get -y autoremove \
 && rm -rf /var/lib/apt/lists/* \
 && mv /etc/bloonix/database/main.conf /etc/bloonix/database/main.conf.default \
 && mv /etc/bloonix/server/main.conf /etc/bloonix/server/main.conf.default \
 && mv /etc/bloonix/srvchk/main.conf /etc/bloonix/srvchk/main.conf.default \
 && mv /etc/bloonix/webgui/main.conf /etc/bloonix/webgui/main.conf.default \
 && cp /etc/bloonix/webgui/nginx.conf /etc/bloonix/webgui/nginx.conf.default

COPY ["./etc", "/etc"]

RUN chmod +x /etc/epicsoft/*.sh \
 && cp /etc/bloonix/webgui/nginx.conf /etc/bloonix.template/nginx.conf

VOLUME [ "/etc/bloonix" ]

EXPOSE 443 \
       5460

ENTRYPOINT ["/etc/epicsoft/entrypoint.sh"]

HEALTHCHECK CMD [ "/etc/epicsoft/healtcheck.sh" ]

ENV BLOONIX_SERVER_USER="bloonix" \
    BLOONIX_SERVER_GROUP="bloonix" \
    BLOONIX_SERVER_TIMEZONE="Europe/Berlin" \
    BLOONIX_SERVER_HOSTNAME="" \
    BLOONIX_SERVER_ALLOW_FROM="127.0.0.1/32" \
    BLOONIX_SERVER_TCP_PORT="5460" \
    BLOONIX_SERVER_TCP_USE_SSL="yes" \
    BLOONIX_SERVER_STATUS_ENABLED="yes" \
    BLOONIX_SERVER_STATUS_ALLOW_FROM="127.0.0.1" \
    BLOONIX_SRVCHK_SSL_VERIFY_MODE="none" \
    BLOONIX_WEBGUI_FCGI_PORT="9000" \
    BLOONIX_WEBGUI_WEBAPP_ENABLE_USER_TRACKING="no"
